
# Kong Ingress Controller Quick Demo

### We deploy complete Kong Environment with Postgresql.
~~~
kubectl create -f kong-ingress-controller/kong-ingress_full-deployment.yml
~~~

### We have created 'kong' namespace so we will wait until all Kong components are ready.
~~~
kubectl get all --namespace kong
~~~

### We deploy now two deployments with their services (red-app and blue-app with red-svc and blue-svc) 
~~~
kubectl create -f colors.yml
~~~

### We review the deployments
~~~
kubectl get all --namespace default
~~~

### We review Kong
~~~
kubectl get all --namespace kong
~~~

### For quick access we prepare some envirnment variables (for Kong admin and Kong proxy access)
~~~
export KONG_ADMIN_PORT=$(minikube service -n kong kong-ingress-controller --url --format "{{ .Port }}")
export KONG_ADMIN_IP=$(minikube service   -n kong kong-ingress-controller --url --format "{{ .IP }}")
export PROXY_IP=$(minikube   service -n kong kong-proxy --url --format "{{ .IP }}" | head -1)
export HTTP_PORT=$(minikube  service -n kong kong-proxy --url --format "{{ .Port }}" | head -1)
export HTTPS_PORT=$(minikube service -n kong kong-proxy --url --format "{{ .Port }}" | tail -1)
~~~

### Now we deploy colors-ingress ingress resource for accesing both applications
~~~
kubectl apply -f kong-ingress-controller/colors-kong-ingress-controller.yml
~~~

### We just set up host routing
~~~
kubectl get ingress kong-colors-ingress -o yaml
~~~

### And now we can access our applications setting host headers
~~~
http ${PROXY_IP}:${HTTP_PORT}/text Host:red.example.com

http ${PROXY_IP}:${HTTP_PORT}/text Host:blue.example.com
~~~

### We review targets because red-app has 3 replicas
~~~
http ${KONG_ADMIN_IP}:${KONG_ADMIN_PORT}/upstreams/default.red-svc.80/targets
~~~

### We now add a KongPluggin resource for reate limitting to route
~~~
kubectl create -f kong-ingress-controller/kong-ratelimiting-plugin.yml 
~~~

### We review deployed Kong Plugins
~~~
kubectl get kongplugins
~~~

### And now we apply rate limit to our deployed ingress resource
~~~
kubectl patch ingress kong-colors-ingress \
  -p '{"metadata":{"annotations":{"rate-limiting.plugin.konghq.com":"add-ratelimiting-to-route\n"}}}'
~~~

### We can review the plugin, associated to route and routes deployed
~~~
http ${KONG_ADMIN_IP}:${KONG_ADMIN_PORT}/plugins

http ${KONG_ADMIN_IP}:${KONG_ADMIN_PORT}/routes
~~~

### Accessing to our applications we can review proxying requests and limits applied
~~~ 
http ${PROXY_IP}:${HTTP_PORT}/text Host:red.example.com

http ${PROXY_IP}:${HTTP_PORT}/text Host:blue.example.com
~~~

### We now delete ingress resource for creating separated ones
~~~ 
kubectl delete ingress kong-colors-ingress
~~~

### Deploying different ingress resources
~~~
kubectl create -f kong-ingress-controller/red-kong-ingress-controller.yml

kubectl create -f kong-ingress-controller/blue-kong-ingress-controller.yml
~~~

### Review Ingress resources
~~~
kubectl get ingress
~~~

### Applying pluging just for one service
~~~
kubectl patch svc red-svc \
  -p '{"metadata":{"annotations":{"rate-limiting.plugin.konghq.com": "add-ratelimiting-to-route\n"}}}'
~~~

### And we review now the 
~~~
http ${KONG_ADMIN_IP}:${KONG_ADMIN_PORT}/plugins
~~~

### And its access
~~~ 
http ${PROXY_IP}:${HTTP_PORT}/text Host:red.example.com
~~~